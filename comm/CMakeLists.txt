# SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later


# Communication library --------------------------------------------------------

set( HEADER_FILES
     ./inc
     ../contrib/miLoCo/inc
     ../contrib/tlv-mi/inc
    )

set( SRC_FILES
    ./src/RemoteCtrlServer.cpp
    )

add_library( communication STATIC ${SRC_FILES})

target_include_directories( communication PUBLIC ${HEADER_FILES})
