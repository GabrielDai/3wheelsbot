/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <miloco/tasks/Task.h>

#include "comm/Comm.h"


/**
 * Task template for communications routines
 * (tlv-mi::EncodeRoutine and tlv-mi::DecodeRoutine).
 */
template<typename routine_t>
class Comm::CommTask: public Miloco::Task
{
    public:
        explicit CommTask(
                std::chrono::microseconds delay,
                routine_t routine) :
            Task(delay),
            mRoutine(routine) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            mRoutine.run();
        }

    private:
        routine_t mRoutine;
};
