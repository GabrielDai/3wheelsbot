/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <algorithm>
#include <array>
#include <arpa/inet.h>
#include <cstdint>
#include <sys/socket.h>
#include <unistd.h>

#include "comm/Comm.h"
#include "comm/CommProperties.h"


/**
 * Auxiliary functions to receive and to send from a socket. The reception
 * saves the bytes into BuffersManager.
 */
struct Comm::SocketTransceiver
{
    /**
     * File descriptor of the socket
     */
    int socketFd = 0;

    /**
     * Receive bytes from other side
     *
     * \return amount of read bytes
     */
    template<class it_t>
    ssize_t obtainBytes(it_t begin, std::size_t size) {
        auto rxBytes = read(socketFd, &begin[0], size);
        return rxBytes;
    }

    /**
     * Send bytes of BuffersManager
     */
    template<typename buffers_mgr_t>
    void deliverBytes(buffers_mgr_t& bufferMgr) {
        std::size_t buffer = 0;
        while (buffer < buffers_mgr_t::buffers) {
            std::array<uint8_t, Comm::bufferSize> msg;
            auto end = std::min(msg.size(), bufferMgr.getPendingBytes(buffer));

            for (std::size_t i = 0; i < end; i++) {
                auto byte = bufferMgr.getByte(buffer);
                msg[i] = byte;
            }
            if (bufferMgr.getPendingBytes(buffer) == 0) {
                buffer++;
            }

            send(socketFd , msg.data(), end, 0);
        }
    }
};
