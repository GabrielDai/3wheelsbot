/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>


/**
 * Forward declarations of namespace Comm (Communication)
 */
namespace Comm
{
    // Entities
    enum class MsgType: uint8_t;
    class SmallProtocol;
    class SmallProtocolDecoder;

    struct SocketTransceiver;
    template<typename routine_t>
    class CommTask;
    class RemoteCtrlServer;
}
