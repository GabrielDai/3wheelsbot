/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <iostream>
#include <memory>

#include "comm/CommProperties.h"


namespace Comm
{
    template<typename entity_t>
    void send(MsgType type, entity_t entity) {
        auto length = entity.bufferLength;
        auto value = entity.serializeIntoBuffer();
        auto tlv = std::make_unique<TLVMi::TLV<MsgType>>(type, length, std::move(value));

        if(!tlvManagerTx.pushTLV(std::move(tlv))) {
            std::cerr << "[Encoding] Producing data faster than consuming" << std::endl;
        }
    }
}
