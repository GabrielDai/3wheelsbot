/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <functional>

#include "comm/Comm.h"
#include "comm/SmallProtocol.h"


/**
 * Class for Small protocol decoding
 */
class Comm::SmallProtocolDecoder
{
    /**
     * States that the decoder can take
     */
    enum class State: uint8_t {
        controlProtocolVersion,
        getType,
        getLength,
        getPayload,
        eraseData,
    };

    public:
        explicit SmallProtocolDecoder() :
            mProtocolVersion(SmallProtocol::protocolVersion),
            mState(State::controlProtocolVersion) {
        }

        /**
         * Routine for decoding. It saves the bytes of value into the
         * corresponding buffer using saveByte(). Once a message was received
         * completely, the function registerCompleteMsg() is called
         */
        template<class it_t>
        void decode(
                it_t begin,
                std::size_t length,
                std::function<void(std::size_t, uint8_t)> saveByte,
                std::function<void(std::size_t)> registerCompleteMsg) {

            for(std::size_t i = 0; i < length; i++) {
                auto byte = *(begin + i);

                switch(mState) {
                    case State::controlProtocolVersion:
                        if(byte != mProtocolVersion) {
                            // Erase data
                            mState = State::eraseData;
                        } else {
                            mState = State::getType;
                        }
                        break;

                    case State::getType:
                        mCurrentType = byte;
                        mState = State::getLength;
                        break;

                    case State::getLength:
                        mCurrentLength = byte;
                        saveByte(mCurrentType, byte);
                        if (mCurrentLength != 0) {
                            mState = State::getPayload;
                        } else {
                            mState = State::controlProtocolVersion;
                            registerCompleteMsg(mCurrentType);
                        }
                        break;

                    case State::getPayload:
                        saveByte(mCurrentType, byte);
                        mCurrentLength--;
                        if (mCurrentLength == 0) {
                            mState = State::controlProtocolVersion;
                            registerCompleteMsg(mCurrentType);
                        }
                        break;

                    case State::eraseData:
                        for(mCurrentLength = *(begin + i + 1); mCurrentLength > 0; mCurrentLength--) {
                            i++;
                        }
                        mState = State::controlProtocolVersion;
                        break;
                }
            }
        }

    private:
        uint8_t mProtocolVersion;
        State mState;
        std::size_t mCurrentType;
        std::size_t mCurrentLength;
};
