/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "comm/Comm.h"
#include "tlv-mi/TLVManager.h"
#include "tlv-mi/BuffersManager.h"
#include "tlv-mi/TLVDispatcher.h"


namespace Comm
{
    /**
     * Defines the different types of messages can be transmitted
     */
    enum class MsgType: uint8_t {
        ABORT,
        MOTION_CMD,
        YAW_LOG,
        MOTORS_VIN_LOG,
        MOTORS_REV_LOG,
        MOTORS_VIN,
        MOTORS_REV,
        DISTANCE_RQS,
        DISTANCE_RES,
        CLOSE_CONNECTION,
        END
    };

    /*
     * Definitions of TLVManager and BuffersManager types from templates of
     * tlv-mi
     */
    constexpr std::size_t bufferSize = 4096;
    using tlv_mgr_t = TLVMi::TLVManager<MsgType, bufferSize, static_cast<std::size_t>(MsgType::END)>;
    using buffer_mgr_t = TLVMi::BuffersManager<bufferSize, static_cast<std::size_t>(MsgType::END)>;

    /**
     * TLVManager objects for transmission and reception
     */
    extern tlv_mgr_t tlvManagerTx;
    extern tlv_mgr_t tlvManagerRx;

    /**
     * Definition of TLVDispatcher
     */
    using dispatcher_t = TLVMi::TLVDispatcher<tlv_mgr_t, MsgType>;
}
