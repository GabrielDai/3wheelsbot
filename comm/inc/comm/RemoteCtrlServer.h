/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <arpa/inet.h>
#include <cstdint>
#include <miloco/posix/TaskExecuter.h>
#include <sys/socket.h>
#include <tlv-mi/EncodeRoutine.h>
#include <tlv-mi/DecodeRoutine.h>

#include "comm/Comm.h"
#include "comm/CommProperties.h"
#include "comm/SmallProtocol.h"
#include "comm/SmallProtocolDecoder.h"
#include "comm/SocketTransceiver.h"
#include "comm/CommTask.h"


/**
 * Create a TCP service.
 */
class Comm::RemoteCtrlServer
{
    using encode_routine_t = TLVMi::EncodeRoutine<
        tlv_mgr_t,
        buffer_mgr_t,
        SmallProtocol,
        SocketTransceiver&>;

    using decode_routine_t = TLVMi::DecodeRoutine<
        tlv_mgr_t,
        buffer_mgr_t,
        SmallProtocol,
        SocketTransceiver&,
        bufferSize * 3>;

    public:
        explicit RemoteCtrlServer(uint32_t port);
        ~RemoteCtrlServer();

        /**
         * Open a TCP socket
         *
         * \return true if success
         */
        bool createService();

        /**
         * Wait client to connect
         *
         * \return true if success
         */
        bool waitClient();

    private:
        /**
         * This method is called after one client is connected in order to
         * start the communication routines
         */
        void startCommRoutines();

        struct sockaddr_in mServerAddr;
        int mOpt;
        int mServerAddrLen;
        int mServerSocket;
        int mClientSocket;
        int mPossibleClients;
        bool mServerSocketOpen;
        bool mClientSocketOpen;

        SocketTransceiver mTransceiver;
        encode_routine_t mEncodeRoutine;
        decode_routine_t mDecodeRoutine;
        CommTask<encode_routine_t&> mEncodeTask;
        CommTask<decode_routine_t&> mDecodeTask;
        Miloco::Posix::TaskExecuter mEncodingExecuter;
        Miloco::Posix::TaskExecuter mDecodingExecuter;
        bool mEncodingExecuterStarted;
        bool mDecodingExecuterStarted;
};
