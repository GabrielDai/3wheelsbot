/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <functional>

#include "comm/Comm.h"


/**
 * It is minimal implementation of protocol needed by tlv-mi.
 *
 * It only contains a function, encode(), and no object can be created from
 * this class.
 */
class Comm::SmallProtocol
{
    public:
        using decoder_t = SmallProtocolDecoder;
        static constexpr uint8_t protocolVersion = 0;
        static constexpr std::size_t amountOfExtraBytes = 3;

        /**
         * Encode a TLV into a buffer
         *
         * Byte[0] Protocol version
         * Byte[1] Type
         * Byte[2] Length
         * Byte[3 ...] Bytes that represent the Value
         */
        template<typename tlv_t>
        static void encode(std::function<void(uint8_t byte)> saveIntoBuffer, tlv_t tlv) {
            saveIntoBuffer(protocolVersion);
            saveIntoBuffer(static_cast<uint8_t>(tlv->type)); // WARNING: type is type_t
            saveIntoBuffer(static_cast<uint8_t>(tlv->length)); // WARNING: length is size_t
            for(std::size_t i = 0; i < tlv->length; i++) {
                saveIntoBuffer(tlv->buffer[i]);
            }
        }

    private:
        SmallProtocol();
};
