/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <algorithm>
#include <iostream>
#include <vector>
#include <unistd.h>
#include <comm/RemoteCtrlServer.h>

using namespace std::chrono_literals;

namespace Comm {
    tlv_mgr_t tlvManagerTx;
    tlv_mgr_t tlvManagerRx;
}

Comm::RemoteCtrlServer::RemoteCtrlServer(uint32_t port) :
    mServerAddr{
        AF_INET,
        htons(port),
        INADDR_ANY},
    mOpt(1),
    mServerAddrLen(sizeof(mServerAddr)),
    mPossibleClients(1),
    mServerSocketOpen(false),
    mClientSocketOpen(false),
    mEncodeRoutine(tlvManagerTx, mTransceiver),
    mDecodeRoutine(tlvManagerRx, mTransceiver),
    mEncodeTask(30ms, mEncodeRoutine),
    mDecodeTask(30ms, mDecodeRoutine),
    mEncodingExecuter(mEncodeTask),
    mDecodingExecuter(mDecodeTask),
    mEncodingExecuterStarted(false),
    mDecodingExecuterStarted(false)
{
}

Comm::RemoteCtrlServer::~RemoteCtrlServer()
{
    if (mEncodingExecuterStarted) {
        mEncodingExecuter.stop(100ms);
    }

    if (mDecodingExecuterStarted) {
        mDecodingExecuter.stop(100ms);
    }

    if (mClientSocketOpen) {
        close(mClientSocket);
    }

    if (mServerSocketOpen) {
        close(mServerSocket);
    }
}

bool
Comm::RemoteCtrlServer::createService()
{
    int result = -1;

    mServerSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(mServerSocket == -1) {
        std::cerr << "Error while calling socket()" << std::endl;
        return false;
    }
    mServerSocketOpen = true;

    result = setsockopt(
            mServerSocket,
            SOL_SOCKET,
            SO_REUSEADDR | SO_REUSEPORT,
            &mOpt,
            sizeof(mOpt));
    if(result == -1) {
        std::cerr << "Error while calling setsockopt()" << std::endl;
        return false;
    }

    result = bind(
            mServerSocket,
            (struct sockaddr *) &mServerAddr,
            sizeof(mServerAddr));
    if(result == -1) {
        std::cerr << "Error while calling bind()" << std::endl;
        return false;
    }

    result = listen(mServerSocket, mPossibleClients);
    if(result == -1) {
        std::cerr << "Error while calling listen()" << std::endl;
        return false;
    }

    return true;
}

bool
Comm::RemoteCtrlServer::waitClient()
{
    int result;

    std::cout << "Waiting client..." << std::endl;
    mClientSocket = accept(
            mServerSocket,
            (struct sockaddr *) &mServerAddr,
            (socklen_t*) &mServerAddrLen);
    if(mClientSocket != -1) {
        result = true;
        mClientSocketOpen = true;
        std::cout << "Client connected" << std::endl;

        startCommRoutines();
    } else {
        result = false;
        std::cerr << "Error while calling accept()" << std::endl;
    }

    return result;
}

void
Comm::RemoteCtrlServer::startCommRoutines()
{
    mTransceiver.socketFd = mClientSocket;

    auto result = mEncodingExecuter.start(SCHED_FIFO, 30);
    if (result == 0) {
        mEncodingExecuterStarted = true;
    }

    result = mDecodingExecuter.start(SCHED_FIFO, 30);
    if (result == 0) {
        mDecodingExecuterStarted = true;
    }
}
