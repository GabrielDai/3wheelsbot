/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cmath>

#include "kinematics/Kinematics.h"


struct Kinematics::Wheel
{
    Wheel(
            int pulses,
            float wheelRadius) :
        pulsesPerTurn(pulses),
        radPerPulse(2 * M_PI / pulses),
        radius(wheelRadius) {
    }

    int pulsesPerTurn;
    float radPerPulse;
    float radius;
};
