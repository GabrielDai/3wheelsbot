/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cmath>
#include <utility>

#include "kinematics/Kinematics.h"
#include "kinematics/Wheel.h"


/*
 * Kinematics model of the 3WheelsBot
 */
template<typename imu_t>
class Kinematics::TWheelsBot
{
    public:
        TWheelsBot(
                float distBetweenWheels,
                Kinematics::Wheel wheel,
                imu_t& imu) :
            m_halfDistBtwnWheels(distBetweenWheels / 2.),
            m_wheel(wheel),
            m_imu(imu) {
        }

        /**
         * Calculates the speed of each wheel based on the desired speed
         * referenced from the inertial reference frame, i.e. the inverse
         * kinematics
         */
        std::pair<float, float>
        calcSpeedWheels(float vX, float vY, float vO) {
            auto currentYaw = m_imu.getYaw();
            currentYaw = 0; // TODO to remove this line the value of yaw should be stable
            auto t = vX * cos(currentYaw) + vY * sin(currentYaw);
            auto angleContrib = vO * m_halfDistBtwnWheels;
            auto wheelDiam = 2 * m_wheel.radius;

            std::pair<float, float> speedWheels;
            speedWheels.first = (t - angleContrib) / wheelDiam; // Left wheel
            speedWheels.second = (t + angleContrib) / wheelDiam; // Right wheel

            // Quantization
            speedWheels.first = std::round(speedWheels.first / m_wheel.radPerPulse) * m_wheel.radPerPulse;
            speedWheels.second = std::round(speedWheels.second / m_wheel.radPerPulse) * m_wheel.radPerPulse;

            return speedWheels;
        }

    private:
        float m_halfDistBtwnWheels;
        Kinematics::Wheel m_wheel;
        imu_t& m_imu;
};
