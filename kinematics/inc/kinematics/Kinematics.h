/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

/**
 * Forward declarations of namespace Kinematics
 */
namespace Kinematics
{
    // Entities
    struct Wheel;

    template<typename imu_t>
    class TWheelsBot;
}
