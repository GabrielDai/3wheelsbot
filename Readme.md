# 3Wheelsbot

It's a bot based on [beaglebone blue](http://beagleboard.org/blue) that is
compatible with ROS2 (for more info. visit
[twheelsbot](https://gitlab.com/GabrielDai/twheelsbot)).

The goal of this project is to show an use case of
[miLoCo](https://gitlab.com/GabrielDai/miloco). For an introduction to miLoCo
[visit](https://gabrieldai.gitlab.io/posts/an-overview-miloco/).

For reading more about this project, take a look at my
[blog](https://gabrieldai.gitlab.io/posts/miloco-3wheelsbot/).

## Building

**Note:** the toolchain for arm is needed.

First step is to create a folder for building the targets. Then we should call
`cmake` as follow:

``` shell
mkdir build
cd build
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=/path/to/toolchain ..
```

Finally call the target you want to build, e.g.:

``` shell
make -j motion-control
```

After compiling the desired program, you should copy it to the beaglebone
(you can use `scp`).

### Programs

* **motor-step-response**: used to analyze the step response of a motor
* **motor-simulation**: used to simulate the control loop of the motor modelled by the program from above
* **motion-control**: used to control the bot movements

## Analysing the step response of a DC motor

You should run `motor-step-reponse > step_output` on beaglebone. This saves the output
on a file called step_output. Then run `stepResponse.py` script, which is under the
folder scripts, to analyze the output.

## Moving the bot

For controlling the bot you have 2 alternative for now ;), both are running
**motion-control**:
1. `./motion-control` (without arguments) will indicate to the bot to travel a
hardcoded path.
2. `./motion-control rem` will start a server and wait until a client be
connected that sends the motion commands to the bot. The `remoteControl.py`
script is an example of a client, with it you can control the bot with
the arrows of your keyboard.

### Fine motor adjustment

Because the motors are not exactly the same, we need to fine tune the control.
You can achieve that by doing:
1. Run `./motion-control rem x.x x.x`, where x.x are the factors for the left and
the right motor respectively, e.g. `./motion-control rem 30.5 32.3`
2. While running `remoteControl.py`, sending a **forward command** (`up arrow`)
and then a **request measurement command** (`m`). The travelled distance of each
wheel should be approximately the same
3. Change the factors of point 1, until you are satisfied with the result
