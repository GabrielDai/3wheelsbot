#!python3
#
# SPDX-FileCopyrightText: 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import argparse
import sys
from robotControl import app

if __name__ == "__main__":
    # Parsing arguments
    args_parser = argparse.ArgumentParser(description='Simple 3WheelsBot remote controlling app.')
    args_parser.add_argument("ip_addr", help="ip address of the robot")
    args_parser.add_argument("-p", "--port",type=int, nargs="?", default=65000, help="TCP port")
    arguments = args_parser.parse_args(sys.argv[1:])

    with app.App(arguments.ip_addr, arguments.port) as remote_control:
        remote_control.run()
