#!python3
#
# SPDX-FileCopyrightText: 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import socket
import struct
from enum import IntEnum
from .tlvmipy import managers


class MsgType(IntEnum):
    ABORT = 0
    MOTION_CMD = 1
    YAW_LOG = 2
    MOTORS_VIN_LOG = 3
    MOTORS_REV_LOG = 4
    MOTORS_VIN = 5
    MOTORS_REV = 6
    DISTANCE_RQS = 7
    DISTANCE_RES = 8
    CLOSE_CONNECTION = 9


buffers = len(MsgType)
tlv_man_rx = managers.TLVManager(buffers)
tlv_man_tx = managers.TLVManager(buffers)


def send(tlv_type, entity):
    length = entity.get_length()
    if length != 0:
        value = entity.serialize_into_buffer()
    else:
        value = bytearray()
    tlv = managers.TLV(tlv_type, length, value)
    if not tlv_man_tx.push_tlv(tlv):
        print("[Encoding] Producing data faster than consuming\r")


class MotionCmd:

    def __init__(self, dX, dY, dO, dt):
        self.dX = dX
        self.dY = dY
        self.dO = dO
        self.dt = dt

    def get_length(self):
        return 16

    def serialize_into_buffer(self):
        payload = bytearray(struct.pack(
                "ffff",
                self.dX,
                self.dY,
                self.dO,
                self.dt))

        return payload


class WheelsMeasurement:

    def __init__(self, left_wheel, right_wheel):
        self.left_wheel = left_wheel
        self.right_wheel = right_wheel

    def deserialize(tlv):
        left_wheel = struct.unpack("f", tlv.value[0:4])[0]
        right_wheel = struct.unpack("f", tlv.value[4:8])[0]

        measurement = WheelsMeasurement(left_wheel, right_wheel)
        return measurement


class SingleCmd:
    def get_length(self):
        return 0;


class OneFloat:

    def deserialize(tlv):
        value = struct.unpack("f", tlv.value[0:4])[0]
        return value


class ClientTCP:

    def __init__(self, ip_addr, port):
        self.ip_addr = ip_addr
        self.port = port

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.ip_addr, self.port))

    def disconnect(self):
        self.socket.close()

    def deliver_bytes(self, buffers_manager):
        for i in range(len(buffers_manager.buffers)):
            if buffers_manager.buffers[i]:
                buff = buffers_manager.buffers[i]
                buffers_manager.buffers[i] = bytearray()
                self.socket.sendall(buff)

    def obtain_bytes(self):
        rx_bytes = self.socket.recv(4096)
        return rx_bytes


class SmallProtocol:

    def __init__(self):
        self.protocol_version = 0

    def encode(self, save, tlv):
        save(self.protocol_version)
        save(tlv.tlv_type)
        save(tlv.length)
        for byte in tlv.value:
            save(byte)

    class Decoder:

        def __init__(self):
            self.protocol_version = 0
            self.phase = "protocol_version"

            self.decode_phase = {}
            self.decode_phase["protocol_version"] = self.check_protocol
            self.decode_phase["type"] = self.get_type
            self.decode_phase["length"] = self.get_length
            self.decode_phase["payload"] = self.get_payload

        def decode(self, rx_bytes, save, register):
            for byte in rx_bytes:
                self.decode_phase[self.phase](byte, save, register)

        def check_protocol(self, byte, save, register):
            if byte == self.protocol_version:
                self.erase_data = False
            else:
                self.erase_data = True
            self.phase = "type"

        def get_type(self, byte, save, register):
            self.tlv_type = byte
            self.phase = "length"

        def get_length(self, byte, save, register):
            self.length = byte
            save(self.tlv_type, byte)
            if self.length != 0:
                self.phase = "payload"
            else:
                self.phase = "protocol_version"

        def get_payload(self, byte, save, register):
            self.length -= 1
            if self.erase_data == False:
                save(self.tlv_type, byte)
            if self.length == 0:
                if self.erase_data == False:
                    register(self.tlv_type)
                self.phase = "protocol_version"
