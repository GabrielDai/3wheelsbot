#!python3
#
# SPDX-FileCopyrightText: 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import curses
import time
from threading import Thread
from . import comm
from .tlvmipy import routines
from .tlvmipy import managers


class Runable:
    def __init__(self, routine, delay):
        self.routine = routine
        self.delay = delay
        self.running = True

    def __call__(self):
        while self.running:
            self.routine()
            time.sleep(self.delay)

    def stop(self):
        self.running = False


class App:
    def __init__(self, ip_addr, port, ros_bridge=None):
        self.client_tcp = comm.ClientTCP(ip_addr, port)
        self.ros_bridge = ros_bridge
        self.current_yaw = 0.0

        protocol = comm.SmallProtocol()
        encode = routines.Encode(comm.tlv_man_tx, protocol, self.client_tcp)
        decode = routines.Decode(comm.tlv_man_rx, protocol, self.client_tcp)
        self.encoding = Runable(encode, 0.1)
        self.decoding = Runable(decode, 0.1)
        self.tx_thread = Thread(target=self.encoding, args=())
        self.rx_thread = Thread(target=self.decoding, args=())
        self.dispatcher = managers.TLVDispatcher(comm.tlv_man_rx)

        self.register_cmds()
        self.register_callbacks()

    def __enter__(self):
        self.init()
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.close()

    def init(self):
        self.client_tcp.connect()
        self.tx_thread.start()
        self.rx_thread.start()

        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(True)
        self.stdscr.nodelay(True)

        self.running = True

    def register_cmds(self):
        self.commands = {}
        self.commands[curses.KEY_UP] = self.cmd_go_forward
        self.commands[curses.KEY_DOWN] = self.cmd_go_backward
        self.commands[curses.KEY_LEFT] = self.cmd_turn_left
        self.commands[curses.KEY_RIGHT] = self.cmd_turn_right
        self.commands[ord(" ")] = self.cmd_abort
        self.commands[ord("m")] = self.cmd_distances_rqs
        self.commands[ord("v")] = self.cmd_motors_vin_log
        self.commands[ord("r")] = self.cmd_motors_rev_log
        self.commands[ord("q")] = self.cmd_exit

    def register_callbacks(self):
        self.dispatcher.subscribe_callback(comm.MsgType.DISTANCE_RES, self.print_distance)
        self.dispatcher.subscribe_callback(comm.MsgType.MOTORS_VIN, self.save_motors_vin)
        self.dispatcher.subscribe_callback(comm.MsgType.MOTORS_REV, self.save_motors_rev)
        self.dispatcher.subscribe_callback(comm.MsgType.YAW_LOG, self.save_yaw)

    def print_distance(self, tlv):
        distances = comm.WheelsMeasurement.deserialize(tlv)
        print("Travelled distance: {}[left wheel] {}[right wheel]\r".format(
                distances.left_wheel, distances.right_wheel))

    def save_motors_vin(self, tlv):
        if self.ros_bridge is not None:
            if len(self.ros_bridge.motors_vin) > 100:
                self.ros_bridge.motors_vin.pop(0)

            motors_vin = comm.WheelsMeasurement.deserialize(tlv)
            self.ros_bridge.motors_vin.append(motors_vin)

    def save_motors_rev(self, tlv):
        if self.ros_bridge is not None:
            if len(self.ros_bridge.motors_rev) > 100:
                self.ros_bridge.motors_rev.pop(0)

            motors_rev = comm.WheelsMeasurement.deserialize(tlv)
            self.ros_bridge.motors_rev.append(motors_rev)

    def save_yaw(self, tlv):
        self.current_yaw = comm.OneFloat.deserialize(tlv)
        if self.ros_bridge is not None:
            self.ros_bridge.current_yaw = self.current_yaw

    def close(self):
        # wait half a second to give time to encoding to dispatch remaining messages
        time.sleep(0.5)
        self.encoding.stop()
        self.decoding.stop()
        self.tx_thread.join()
        self.rx_thread.join()
        self.client_tcp.disconnect()

        self.stdscr.keypad(False)
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    def execute_tasks(self):
        # Handle pressed keys
        key = self.stdscr.getch()
        if key in self.commands:
            self.commands[key]()

        # Handle received messages
        self.dispatcher.verify_and_distpatch()

        # Handle received motion commands from the ros2 bridge
        if self.ros_bridge is not None:
            cmd_list = self.ros_bridge.motion_cmds
            while cmd_list:
                cmd = cmd_list.pop(0)
                comm.send(comm.MsgType.MOTION_CMD, cmd)

    def run(self):
        while self.running:
            self.execute_tasks()

    def cmd_go_forward(self):
        print("Forward\r")
        cmd = comm.MotionCmd(0.3, 0, 0, 1) # TODO calculates vX and vY based on the current yaw
        comm.send(comm.MsgType.MOTION_CMD, cmd)

    def cmd_go_backward(self):
        print("Backward\r")
        cmd = comm.MotionCmd(-0.3, 0, 0, 1) # TODO calculates vX and vY based on the current yaw
        comm.send(comm.MsgType.MOTION_CMD, cmd)

    def cmd_turn_left(self):
        print("Turning Left\r")
        cmd = comm.MotionCmd(0, 0, 1.5708, .7)
        comm.send(comm.MsgType.MOTION_CMD, cmd)

    def cmd_turn_right(self):
        print("Turning Right\r")
        cmd = comm.MotionCmd(0, 0, -1.5708, .7)
        comm.send(comm.MsgType.MOTION_CMD, cmd)

    def cmd_distances_rqs(self):
        print("Request travelled distance per wheel\r")
        comm.send(comm.MsgType.DISTANCE_RQS, comm.SingleCmd())

    def cmd_motors_vin_log(self):
        print("Toggling motors Vin logs\r")
        comm.send(comm.MsgType.MOTORS_VIN_LOG, comm.SingleCmd())

    def cmd_motors_rev_log(self):
        print("Toggling motors revolution logs\r")
        comm.send(comm.MsgType.MOTORS_REV_LOG, comm.SingleCmd())

    def cmd_abort(self):
        print("Abort\r")
        comm.send(comm.MsgType.ABORT, comm.SingleCmd())

    def cmd_exit(self):
        comm.send(comm.MsgType.CLOSE_CONNECTION, comm.SingleCmd())
        self.running = False
