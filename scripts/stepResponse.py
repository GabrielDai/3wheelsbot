#!python3
#
# SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import argparse
import os
import sys
import re
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal


def moving_average(a, n):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


class StepResponseParser:
    def __init__(self):
        self.delay_re = re.compile(r"delay:(\d+)")
        self.batt_re = re.compile(r"battery voltage:(\d+.\d+)")
        self.output_re = re.compile(r"(\w+):((-*\d+:)+)")

    def parse_line(self, line):
        match = self.delay_re.search(line)
        if match != None:
            self.delay = float(match[1])
        match = self.batt_re.search(line)
        if match != None:
            self.battery_voltage = float(match[1])
        match = self.output_re.search(line)
        if match != None:
            self.measurements = match[2].split(":")

    def get_measurements(self):
        if hasattr(self, "measurements") and hasattr(self, "battery_voltage"):
            position_samples = [math.fabs(int(m))/self.battery_voltage for m in self.measurements[:-1]]
            speed_samples = [position_samples[n+1] - position_samples[n] for n in range(len(position_samples) - 1)]
        else:
            speed_samples = None
        return speed_samples

    def get_battery_voltage(self):
        if hasattr(self, "battery_voltage"):
            batt = self.battery_voltage
        else:
            batt = None
        return batt

    def get_sampling_time(self):
        if hasattr(self, "delay"):
            sampling_time = self.delay * 1e-6
        else:
            sampling_time = None
        return sampling_time


class PlantModel:
# https://en.wikipedia.org/wiki/Step_response
    def __init__(self, k, t25, t75):
        self.k = k # steady-state gain
        r = t25 / t75 # time to 25% / time to 75%
        P = 18.56075 * r - (0.57311 / (r - 0.20747)) - 4.16423
        X = 14.2797 * r**3 - 9.3891 * r**2 + 0.25437 * r + 1.32148
        self.T2 = (t75 - t25) / (X * (1 + 1/P))
        self.T1 = self.T2/P

    def get_transfer_function(self):
        num = [self.k]
        den = [self.T1 * self.T2, self.T1 + self.T2, 1]
        return [num, den]

    def get_state_space_matrices(self):
        a1 = self.T1 + self.T2
        a2 = self.T1 * self.T2
        A = [[0, 1], [-1/a2, -a1/a2]]
        B = [[0], [self.k/a2]]
        C = [1, 0]
        # D = [0]
        return (A, B, C)


if __name__ == "__main__":
    # Parsing arguments
    args_parser = argparse.ArgumentParser(description='Parse the output of motor-response.')
    args_parser.add_argument("file_path", type=open, help="path to the file to parse")
    args_parser.add_argument("-s", "--span",type=int, nargs="?", default=1, help="span of the moving average filter")
    args_parser.add_argument("-c", "--curve", type=float, metavar=("<steady-state gain>", "<samples to 25%>", "<samples to 75%>"), nargs=3, help="draw a fitting curve")
    arguments = args_parser.parse_args(sys.argv[1:])

    # Parsing the file
    file_parser = StepResponseParser()
    with arguments.file_path as f:
        for line in f:
            file_parser.parse_line(line)

    speed_samples = file_parser.get_measurements()
    battery_voltage = file_parser.get_battery_voltage()
    sampling_time = file_parser.get_sampling_time()
    if speed_samples != None and sampling_time != None:
        print("Battery Voltage: {}[V]\n".format(battery_voltage))
        speed_samples = moving_average(speed_samples, arguments.span)

        # Fitting the curve
        if arguments.curve != None:
            k = arguments.curve[0]
            t25 = arguments.curve[1] * sampling_time
            t75 = arguments.curve[2] * sampling_time
            motor_model = PlantModel(k, t25, t75)
            [num, den] = motor_model.get_transfer_function()
            print("{:18.4f}\n------------------------------\n {:.4f}s^2 + {:.4f}s + {}\n".format(num[0], den[0], den[1], den[2]))
            (A, B, C) = motor_model.get_state_space_matrices()
            print("A =", A)
            print("B =", B)
            print("C =", C)

        # Plotting the curve
        mpl.style.use('seaborn')
        plt.xlabel("Time")
        plt.ylabel("Speed")

        time = [i * sampling_time for i in range(len(speed_samples))]
        plt.stem(time, speed_samples, use_line_collection=True)

        if "motor_model" in locals():
            G = signal.lti(num, den)
            g, t = signal.step(G)
            plt.plot(g, t, 'C2')
        plt.show()
    else:
        print("\nThe file does not correspond to the output of motor-response or it is corrupted.\n")
