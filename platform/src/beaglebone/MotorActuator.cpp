/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <rc/motor.h>
#include <platform/beaglebone/MotorActuator.h>


using shared_variable_t = Control::SharedVariable<double>;


Beaglebone::MotorActuator::MotorActuator(
        uint8_t motor,
        shared_variable_t& voltage_reader)
    : m_motor(motor),
      m_voltage_reader(voltage_reader)
{
}

void
Beaglebone::MotorActuator::setVrms(double Vin)
{
    auto battery_voltage = m_voltage_reader.readValue();
    auto duty = Vin / battery_voltage;
    rc_motor_set(m_motor, duty);
}

void
Beaglebone::MotorActuator::setFreeSpin()
{
    rc_motor_free_spin(m_motor);
}

void
Beaglebone::MotorActuator::breakMotor()
{
    rc_motor_brake(m_motor);
}
