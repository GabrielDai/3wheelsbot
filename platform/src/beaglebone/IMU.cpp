/*
 * SPDX-FileCopyrightText: © 2021 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <iostream>
#include <platform/beaglebone/IMU.h>


bool
Beaglebone::IMU::start()
{
    bool result = true;
    rc_mpu_config_t conf = rc_mpu_default_config();
    conf.i2c_bus = I2C_BUS;
    conf.gpio_interrupt_pin_chip = GPIO_INT_PIN_CHIP;
    conf.gpio_interrupt_pin = GPIO_INT_PIN_PIN;
	conf.enable_magnetometer = 1;

    if(rc_mpu_initialize_dmp(&mMpuData, conf)){
        std::cerr << "rc_mpu_initialize_failed" << std::endl;
        result = false;
    }

    return result;
}

void
Beaglebone::IMU::stop()
{
    rc_mpu_power_off();
}

double
Beaglebone::IMU::getYaw()
{
    return mMpuData.fused_TaitBryan[TB_YAW_Z];
}

void
Beaglebone::IMU::subscribeCallback(void (*func)(void))
{
    rc_mpu_set_dmp_callback(func);
}
