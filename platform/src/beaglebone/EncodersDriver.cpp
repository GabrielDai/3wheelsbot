/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <algorithm>
#include <utility>
#include <rc/encoder_eqep.h>
#include <platform/beaglebone/EncodersDriver.h>


constexpr uint8_t Beaglebone::EncodersDriver::arraySize;

Beaglebone::EncodersDriver::EncodersDriver()
    : m_encoderReaders{
        Beaglebone::EncoderReader(1),
        Beaglebone::EncoderReader(2),
        Beaglebone::EncoderReader(3)}
{
    int r = rc_encoder_eqep_init();
}

Beaglebone::EncodersDriver::~EncodersDriver()
{
    rc_encoder_eqep_cleanup();
}

Beaglebone::EncoderReader&
Beaglebone::EncodersDriver::getEncoder(uint8_t encoder)
{
    encoder = std::min(encoder, arraySize);

    return m_encoderReaders[encoder];
}

std::pair<
    Beaglebone::EncodersDriver::encoder_it,
    Beaglebone::EncodersDriver::encoder_it>
Beaglebone::EncodersDriver::getEncoderPair(
        uint8_t encoder1,
        uint8_t encoder2)
{
    encoder1 = std::min(encoder1, arraySize);
    encoder2 = std::min(encoder2, arraySize);

    return std::make_pair(
            m_encoderReaders.begin() + encoder1,
            m_encoderReaders.begin() + encoder2);
}
