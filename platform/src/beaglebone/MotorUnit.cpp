/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <platform/beaglebone/MotorUnit.h>


Beaglebone::MotorUnit::MotorUnit(
        Beaglebone::EncoderReader& encoder,
        Beaglebone::MotorActuator& actuator) :
    m_encoder(encoder),
    m_actuator(actuator)
{
}

void
Beaglebone::MotorUnit::setInitialValue()
{
    m_actuator.setVrms(Beaglebone::MotorActuator::InitialValue);
}

void
Beaglebone::MotorUnit::setPlantInput(double Vin)
{
    m_actuator.setVrms(Vin);
}

void
Beaglebone::MotorUnit::calibrate()
{
    m_encoder.resetCounter();
}

int
Beaglebone::MotorUnit::readValue()
{
    return m_encoder.readValue();
}
