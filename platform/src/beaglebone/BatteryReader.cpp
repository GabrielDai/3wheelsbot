/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <rc/adc.h>
#include <platform/beaglebone/BatteryReader.h>


Beaglebone::BatteryReader::BatteryReader()
{
    rc_adc_init();
}

Beaglebone::BatteryReader::~BatteryReader()
{
    rc_adc_cleanup();
}

double
Beaglebone::BatteryReader::readValue() const
{
    return rc_adc_batt();
}
