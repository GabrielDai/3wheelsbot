/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <rc/encoder_eqep.h>
#include <platform/beaglebone/EncoderReader.h>


Beaglebone::EncoderReader::EncoderReader(uint8_t channel)
    : m_channel(channel)
{
}

Beaglebone::EncoderReader::EncoderReader(const Beaglebone::EncoderReader& other)
{
    std::lock_guard<std::mutex> lock(m_access);
    m_channel = other.m_channel;
}

int
Beaglebone::EncoderReader::readValue() const
{
    std::lock_guard<std::mutex> lock(m_access);
    int value = rc_encoder_eqep_read(m_channel);

    return value;
}

void
Beaglebone::EncoderReader::resetCounter()
{
    std::lock_guard<std::mutex> lock(m_access);
    rc_encoder_eqep_write(m_channel, 0);
}
