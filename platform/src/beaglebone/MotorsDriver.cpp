/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <algorithm>
#include <utility>
#include <rc/motor.h>
#include <platform/beaglebone/MotorsDriver.h>


constexpr uint8_t Beaglebone::MotorsDriver::arraySize;

Beaglebone::MotorsDriver::MotorsDriver(shared_variable_t& voltageReader)
    : m_motorActuators{
        Beaglebone::MotorActuator(1, voltageReader),
        Beaglebone::MotorActuator(2, voltageReader),
        Beaglebone::MotorActuator(3, voltageReader),
        Beaglebone::MotorActuator(4, voltageReader)}
{
    rc_motor_init_freq(RC_MOTOR_DEFAULT_PWM_FREQ);
}

Beaglebone::MotorsDriver::~MotorsDriver()
{
    for (uint8_t i = 0; i < m_motorActuators.size(); i++) {
        m_motorActuators[i].setFreeSpin();
    }
    rc_motor_cleanup();
}

Beaglebone::MotorActuator&
Beaglebone::MotorsDriver::getMotorActuator(uint8_t motor)
{
    motor = std::min(motor, arraySize);

    return m_motorActuators[motor];
}

std::pair<
    Beaglebone::MotorsDriver::motor_it,
    Beaglebone::MotorsDriver::motor_it>
Beaglebone::MotorsDriver::getMotorActuatorPair(
        uint8_t motor1,
        uint8_t motor2)
{
    motor1 = std::min(motor1, arraySize);
    motor2 = std::min(motor2, arraySize);

    return std::make_pair(
            m_motorActuators.begin() + motor1,
            m_motorActuators.begin() + motor2);
}
