/*
 * SPDX-FileCopyrightText: © 2021 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <iostream>
#include <signal.h>
#include <time.h>
#include <platform/beaglebone/IMU.h>

static bool running = true;

void
termination_handler(int signum)
{
    (void) signum;
    running = false;
}

int
main (int argc, char* argv[])
{
    int result = 0;

    // Sigaction
    struct sigaction action;
    action.sa_handler = &termination_handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGINT, &action, NULL);

    timespec delay, rem_delay;
    delay = {1,0}; // 1s

    Beaglebone::IMU imu;
    imu.start();
    while(running) {
        std::cout << "Yaw: " << imu.getYaw() << std::endl;
        nanosleep(&delay, &rem_delay);
    }
    imu.stop();

    return result;
}
