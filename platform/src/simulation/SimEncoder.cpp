/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <platform/simulation/SimEncoder.h>


Simulation::SimEncoder::SimEncoder(plant_t& plant)
    : m_plant(plant)
{
}

void
Simulation::SimEncoder::calibrate()
{
}

Eigen::Matrix<Simulation::SimEncoder::data_t, Simulation::SimEncoder::outputs, 1>
Simulation::SimEncoder::readOutput()
{
    auto state = m_plant.getStates();

    auto leg_angles = m_plant.getLastOutput();

    return leg_angles;
}

