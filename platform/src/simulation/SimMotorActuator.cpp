/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <platform/simulation/SimMotorActuator.h>


Simulation::SimMotorActuator::SimMotorActuator(plant_t& plant)
    : m_plant(plant)
{
}

void Simulation::SimMotorActuator::setPlantInput(Eigen::Matrix<data_t, inputs, 1> input)
{
    m_plant.setInput(input);

    // Update output
    m_plant.getOutput(input);
}
