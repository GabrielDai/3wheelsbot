/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <Eigen/Dense>
#include <miloco/filters/StateSpaceSys.h>

#include "platform/simulation/Simulation.h"


/**
 * Simulated encoder
 */
class Simulation::SimEncoder
{
    public:
        static constexpr uint8_t state_vars = 2;
        static constexpr uint8_t inputs = 1;
        static constexpr uint8_t outputs = 1;

        using data_t = float;
        using plant_t = Miloco::StateSpaceSys<state_vars, inputs, outputs, data_t>;

        explicit SimEncoder(plant_t& plant);

        void
        calibrate();

        Eigen::Matrix<data_t, outputs, 1>
        readOutput();

    private:
        plant_t& m_plant;
};
