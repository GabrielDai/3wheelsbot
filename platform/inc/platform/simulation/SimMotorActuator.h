/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <Eigen/Dense>
#include <miloco/filters/StateSpaceSys.h>

#include "platform/simulation/Simulation.h"


/**
 * Simulated motor actuator
 */
class Simulation::SimMotorActuator
{
    public:
        static constexpr uint8_t state_vars = 2;
        static constexpr uint8_t inputs = 1;
        static constexpr uint8_t outputs = 1;

        using data_t = float;
        using plant_t = Miloco::StateSpaceSys<state_vars, inputs, outputs, data_t>;

        explicit SimMotorActuator(plant_t& plant);

        void setPlantInput(Eigen::Matrix<data_t, inputs, 1> input);

    private:
        plant_t& m_plant;
};
