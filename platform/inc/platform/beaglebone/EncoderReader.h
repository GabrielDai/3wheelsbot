/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <mutex>

#include "platform/beaglebone/Beaglebone.h"


/**
 * Used to return the value of the encoder decoder per channel
 */
class Beaglebone::EncoderReader
{
    public:
        explicit EncoderReader(uint8_t channel);
        EncoderReader(const EncoderReader& other);

        int
        readValue() const;
        void
        resetCounter();

    private:
        uint8_t m_channel;
        mutable std::mutex m_access;
};
