/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <cstdint>

#include "platform/beaglebone/Beaglebone.h"
#include "platform/beaglebone/EncoderReader.h"


/**
 *  Encoder driver of the beaglebone blue
 */
class Beaglebone::EncodersDriver
{
    public:
        using encoder_t = Beaglebone::EncoderReader;
        using encoder_it = typename std::array<encoder_t, 2>::iterator;

        explicit EncodersDriver();
        ~EncodersDriver();

        Beaglebone::EncoderReader&
        getEncoder(uint8_t encoder);

        std::pair<encoder_it, encoder_it>
        getEncoderPair(
                uint8_t encoder1,
                uint8_t encoder2);

    private:
        constexpr static uint8_t arraySize = 3;
        std::array<Beaglebone::EncoderReader, arraySize> m_encoderReaders;
};
