/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <cstdint>
#include <control/SharedVariable.h>

#include "platform/beaglebone/Beaglebone.h"
#include "platform/beaglebone/MotorActuator.h"


using shared_variable_t = Control::SharedVariable<double>;


/**
 * Motor driver of the beaglebone blue
 */
class Beaglebone::MotorsDriver
{
    public:
        using motor_t = Beaglebone::MotorActuator;
        using motor_it = typename std::array<motor_t, 2>::iterator;

        explicit MotorsDriver(shared_variable_t& voltageReader);
        ~MotorsDriver();

        Beaglebone::MotorActuator&
        getMotorActuator(uint8_t motor);

        std::pair<motor_it, motor_it>
        getMotorActuatorPair(
                uint8_t motor1,
                uint8_t motor2);

        constexpr static uint8_t arraySize = 4;

    private:
        std::array<Beaglebone::MotorActuator, arraySize> m_motorActuators;
};
