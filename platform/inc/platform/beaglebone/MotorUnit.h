/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "platform/beaglebone/Beaglebone.h"
#include "platform/beaglebone/EncoderReader.h"
#include "platform/beaglebone/MotorActuator.h"


/**
 * This class represents the motor as unit and it is used as adapter
 * for miLoCo tasks
 */
class Beaglebone::MotorUnit
{
    public:
        explicit MotorUnit(
                EncoderReader& encoder,
                MotorActuator& actuator);

        void
        setInitialValue();
        void
        setPlantInput(double Vin);
        void
        calibrate();
        int
        readValue();

    private:
        EncoderReader& m_encoder;
        MotorActuator& m_actuator;
};
