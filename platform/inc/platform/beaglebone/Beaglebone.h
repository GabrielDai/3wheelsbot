/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once


/**
 * Forward declarations of namespace Beaglebone
 */
namespace Beaglebone
{
    // Entities
    class BatteryReader;
    class MotorActuator;
    class MotorsDriver;
    class EncoderReader;
    class EncodersDriver;
    class MotorUnit;
    class IMU;
}
