/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <control/SharedVariable.h>

#include "platform/beaglebone/Beaglebone.h"


using shared_variable_t = Control::SharedVariable<double>;


/**
 * It controls the voltage RMS at the input of a motor
 */
class Beaglebone::MotorActuator
{
    public:
        explicit MotorActuator(
            uint8_t motor,
            shared_variable_t& voltage_reader);

        void
        setVrms(double Vin);
        void
        setFreeSpin();
        void
        breakMotor();

        static constexpr double InitialValue = 0;

    private:
        uint8_t m_motor;
        shared_variable_t& m_voltage_reader;
};
