/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "platform/beaglebone/Beaglebone.h"


/**
 * Wrapper of librobotcontrol functions to read the battery voltage
 */
class Beaglebone::BatteryReader
{
    public:
        explicit BatteryReader();
        ~BatteryReader();

        double
        readValue() const;
};
