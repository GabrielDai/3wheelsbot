/*
 * SPDX-FileCopyrightText: © 2021 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <platform/beaglebone/Beaglebone.h>
#include <rc/mpu.h>


class Beaglebone::IMU
{
    public:
        explicit IMU() = default;
        /*
         * Start with default config and modify based on options
         *
         * \return true if success, false otherwise
         */
        bool start();

        /*
         * Stop the mpu
         */
        void stop();

        /*
         * Returns Yaw angle in radians
         */
        double getYaw();

        /*
         * Subscribe a callback which will be called after a measurement is done
         */
        void subscribeCallback(void (*func)(void));

    private:
        rc_mpu_data_t mMpuData;

        // Bus for Robotics Cape and BeagleboneBlue is 2
        static constexpr int I2C_BUS = 2;
        // Interrupt pin is on gpio3.21
        static constexpr int GPIO_INT_PIN_CHIP = 3;
        static constexpr int GPIO_INT_PIN_PIN = 21;
};
