/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <memory>
#include <tlv-mi/Serializable.h>
#include "control/Control.h"


struct Control::MotionCommand
{
    float dX;
    float dY;
    float dO;
    float dt;

    static MotionCommand create(TLVMi::Serializable<float, 4> obj) {
        MotionCommand cmd = {obj.value[0], obj.value[1], obj.value[2], obj.value[3]};
        return cmd;
    }
};
