/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <chrono>
#include <Eigen/Dense>
#include <miloco/ReducedObserver.h>

#include "control/Control.h"


/**
 * Contains the entities for controlling the powertrain
 */
template<
    std::size_t stateVariables,
    std::size_t inputs,
    std::size_t outputs,
    typename data_t>
struct Control::PowertrainControl
{
    using observer_t = Miloco::ReducedObserver<stateVariables, 2, inputs, outputs, data_t>;
    using k_matrix_t = Eigen::Matrix<data_t, inputs, stateVariables>;
    using pre_gain_t = Eigen::Matrix<data_t, inputs, inputs>;

    explicit PowertrainControl(
            std::chrono::microseconds deltaT,
            observer_t obs,
            k_matrix_t kMatrix,
            pre_gain_t preGain) :
        m_deltaT(deltaT),
        m_obs(obs),
        m_kMatrix(kMatrix),
        m_preGain(preGain) {
    }

    Eigen::Matrix<data_t, stateVariables, 1>
    getStates(Eigen::Matrix<data_t, outputs, 1> plantOutput) {
        return m_obs.getStates(plantOutput);
    }

    void
    updateStates(
            Eigen::Matrix<data_t, inputs, 1> plantInput,
            Eigen::Matrix<data_t, outputs, 1> plantOutput) {
        m_obs.updateStates(plantInput, plantOutput);
    }

    std::chrono::microseconds m_deltaT;
    observer_t m_obs;
    k_matrix_t m_kMatrix;
    pre_gain_t m_preGain;
};
