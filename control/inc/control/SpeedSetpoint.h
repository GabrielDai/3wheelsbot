/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>

#include "control/Control.h"


/**
 * This generic class is used by the Powertrain's thread to take commands from
 * MotionController.
 */
template<typename updater_t>
class Control::SpeedSetpoint
{
    public:
        using setpoint_t = typename updater_t::bi_speed_t;
        using data_t = typename updater_t::data_t;

        explicit SpeedSetpoint(updater_t& updater) :
              m_updater(updater) {
        }

        setpoint_t
        readValue() {
            return m_updater.readValue();
        }

    private:
        updater_t& m_updater;
};
