/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <mutex>
#include <queue>
#include <Eigen/Dense>

#include "control/Control.h"
#include "control/SpeedSamples.h"
#include "control/MotionCommand.h"


/**
 * This generic class is used by the MotionController to give commands to the
 * Powertrain's thread.
 */
template<
    typename speed_t,
    typename kinematics_t>
class Control::SpeedUpdater
{
    public:
        static constexpr uint8_t inputs = 2;
        using data_t = speed_t;
        using bi_speed_t = Eigen::Matrix<speed_t, inputs, 1>;

        explicit SpeedUpdater(
                std::chrono::microseconds deltaT,
                speed_t preGainLeft,
                speed_t preGainRight,
                kinematics_t& kinematics) :
            m_deltaT(deltaT),
            m_preGainLeft(preGainLeft),
            m_preGainRight(preGainRight),
            m_kinematics(kinematics),
            m_currentSpeed{0, 0, 0} {
        }

        bi_speed_t
        readValue() {
            if (m_currentSpeed.counts != 0) {
                m_currentSpeed.counts--;
            } else {
                std::lock_guard<std::mutex> lock(m_queueAccess);
                if (!m_cmdFIFO.empty()) {
                    auto cmd = m_cmdFIFO.front();
                    m_cmdFIFO.pop();
                    m_currentSpeed = calcInverseKin(cmd);
                    m_currentSpeed.counts--;
                } else {
                    m_currentSpeed = {0, 0, 0};
                }
            }

            // Pre-gain is used here
            std::lock_guard<std::mutex> lock2(m_preGainAccess);
            bi_speed_t speed;
            speed << m_currentSpeed.leftWheel * m_preGainLeft,
                     m_currentSpeed.rightWheel * m_preGainRight;

            return speed;
        }

        void
        pushCmd(Control::MotionCommand cmd) {
            std::lock_guard<std::mutex> lock(m_queueAccess);
            m_cmdFIFO.push(cmd);
        }

        void
        stopMotors() {
            std::lock_guard<std::mutex> lock(m_queueAccess);
            // Clear FIFO
            while (!m_cmdFIFO.empty()) {
                m_cmdFIFO.pop();
            }
            m_currentSpeed.counts = 0;
        }

        void
        setPreGain(speed_t preGainLeft, speed_t preGainRight) {
            std::lock_guard<std::mutex> lock(m_preGainAccess);
            m_preGainLeft = preGainLeft;
            m_preGainRight = preGainRight;
        }

    private:
        Control::SpeedSamples
        calcInverseKin(Control::MotionCommand cmd) {
            float vX = cmd.dX / cmd.dt;
            float vY = cmd.dY / cmd.dt;
            float vO = cmd.dO / cmd.dt;
            auto speedWheels = m_kinematics.calcSpeedWheels(vX, vY, vO);

            auto samples = calcSamples(cmd.dt);

            Control::SpeedSamples speedSamples = {speedWheels.first / samples, speedWheels.second / samples, samples};
            std::cout << "w1: " << speedSamples.leftWheel << std::endl;
            std::cout << "w2: " << speedSamples.rightWheel << std::endl;
            std::cout << "samples: " << static_cast<int>(speedSamples.counts) << std::endl;
            std::cout << "--------------------------------------" << std::endl;

            return speedSamples;
        }

        uint32_t calcSamples(float dt) {
            std::chrono::microseconds totalTime;
            totalTime = std::chrono::duration<uint32_t, std::micro>(
                    static_cast<uint32_t>(dt * 1000000));
            uint32_t samples = static_cast<uint32_t>(totalTime / m_deltaT);

            return samples;
        }

        std::mutex m_queueAccess;
        std::mutex m_preGainAccess;
        std::chrono::microseconds m_deltaT;
        speed_t m_preGainLeft;
        speed_t m_preGainRight;
        kinematics_t& m_kinematics;
        Control::SpeedSamples m_currentSpeed;
        std::queue<Control::MotionCommand> m_cmdFIFO;
};
