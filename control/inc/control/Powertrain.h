/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <cstdint>
#include <utility>
#include <Eigen/Dense>
#include <kinematics/Wheel.h>
#include <tlv-mi/Serializable.h>

#include "comm/Send.h"
#include "control/Control.h"
#include "control/PowertrainControl.h"
#include "control/Odometer.h"


/**
 * It is an adapter class which contains everything needed to move the wheel,
 * e.g. motor actuators, encoders, etc. This class also do aditional
 * calculations, i.e. absolute angle position, filtering angle position and
 * speed for each wheel.
 */
template<
    typename motor_t,
    typename encoder_t,
    typename data_t>
class Control::Powertrain
{
    public:
        static constexpr uint8_t stateVariables = 4;
        static constexpr uint8_t inputs = 2;
        static constexpr uint8_t outputs = 2;

        using motor_it = typename std::array<motor_t, outputs>::iterator;
        using encoder_it = typename std::array<encoder_t, inputs>::iterator;
        using control_t = Control::PowertrainControl<stateVariables, inputs, outputs, data_t>;
        using odometer_t = Control::Odometer<outputs, data_t>;

        explicit Powertrain(
                std::pair<motor_it, motor_it> motors,
                std::pair<encoder_it, encoder_it> encoders,
                Kinematics::Wheel wheel) :
            m_vinLog(false),
            m_revLog(false),
            m_leftWheelPos(0),
            m_rightWheelPos(0),
            m_motors(motors),
            m_encoders(encoders),
            m_wheel(wheel),
            m_odometer(wheel) {
        }

        /**
         * Set the calibration input of both motors in order to reset the
         * counters. This is done to start from a well known condition.
         * This is called during setup phase of the Task
         */
        void
        setInitialValue() {
            m_motors.first->setVrms(motor_t::InitialValue);
            m_motors.second->setVrms(motor_t::InitialValue);
        }

        /**
         * Set the input of both motor.
         */
        void
        setPlantInput(Eigen::Matrix<data_t, inputs, 1> inputs) {
            m_motors.first->setVrms(-inputs[0]);
            m_motors.second->setVrms(inputs[1]);

            if (m_vinLog) {
                data_t values[2] = {-inputs[0], inputs[1]};
                TLVMi::Serializable<data_t, 2> motorsVin(values);
                send(Comm::MsgType::MOTORS_VIN, motorsVin);
            }
        }

        /**
         * Calibrate the encoders.
         * This is called during setup phase of the Task
         */
        void
        calibrate() {
            m_leftWheelPos = 0;
            m_rightWheelPos = 0;
            m_encoders.first->resetCounter();
            m_encoders.second->resetCounter();
        }

        /**
         * Returns the current speed of both motors and updates the current
         * absolute position
         */
        Eigen::Matrix<data_t, outputs, 1>
        readOutput() {
            int incrementLeft = m_encoders.first->readValue();
            m_encoders.first->resetCounter();

            int incrementRight = -m_encoders.second->readValue();
            m_encoders.second->resetCounter();

            m_odometer.addDistance(incrementLeft, incrementRight);
            calculateAbsPos(incrementLeft, m_leftWheelPos);
            calculateAbsPos(incrementRight, m_rightWheelPos);

            Eigen::Matrix<data_t, outputs, 1> currentSpeed;
            currentSpeed << static_cast<data_t>(incrementLeft),
                            static_cast<data_t>(incrementRight);
            currentSpeed *= m_wheel.radPerPulse;

            if (m_revLog) {
                data_t values[2] = {currentSpeed[0], currentSpeed[1]};
                TLVMi::Serializable<data_t, 2> motorsRev(values);
                send(Comm::MsgType::MOTORS_REV, motorsRev);
            }

            return currentSpeed;
        }

        /**
         * Returns the current absolute position of both motors
         */
        Eigen::Matrix<data_t, outputs, 1>
        readCurrentPoint() {
            Eigen::Matrix<data_t, outputs, 1> positions;
            positions << static_cast<data_t>(m_leftWheelPos),
                         static_cast<data_t>(m_rightWheelPos);
            return positions * m_wheel.radPerPulse;
        }

        odometer_t&
        getOdometer() {
            return m_odometer;
        }

        /**
         * Enable/disable motors voltage input logging
         */
        void
        enableVinLogging(bool enable) {
            m_vinLog = enable;
        }

        /**
         * Enable/disable motors revolution logging
         */
        void
        enableRevLogging(bool enable) {
            m_revLog = enable;
        }

    private:
        void
        calculateAbsPos(int increment, int& currentPos) {
            currentPos += increment;
            currentPos %= m_wheel.pulsesPerTurn;

            if(currentPos < 0) {
                currentPos += m_wheel.pulsesPerTurn;
            }
        }

        bool m_vinLog;
        bool m_revLog;
        int m_leftWheelPos;
        int m_rightWheelPos;
        std::pair<motor_it, motor_it> m_motors;
        std::pair<encoder_it, encoder_it> m_encoders;
        Kinematics::Wheel m_wheel;
        odometer_t m_odometer;
};
