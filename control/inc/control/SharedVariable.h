/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <mutex>

#include "control/Control.h"


/**
 * Thread-safe generic variable
 */
template<typename data_t>
class Control::SharedVariable
{
    public:
        explicit SharedVariable() {
        }

        data_t
        readValue() const {
            std::lock_guard<std::mutex> lock(m_access);
            return m_value;
        }

        void
        updateValue(data_t new_value) {
            std::lock_guard<std::mutex> lock(m_access);
            m_value = new_value;
        }

    private:
        data_t m_value;
        mutable std::mutex m_access;
};
