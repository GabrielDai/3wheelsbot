/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>

#include "control/Control.h"


/**
 * It saves an amount of speed samples. The powertrain's control loop uses these
 * samples as setpoints
 */
struct Control::SpeedSamples
{
    float leftWheel;
    float rightWheel;
    uint32_t counts;
};
