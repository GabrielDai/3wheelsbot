/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cmath>
#include <chrono>
#include <memory>
#include <miloco/tasks/ReducedObsPolePlacement.h>
#include <miloco/posix/TaskExecuter.h>

#include "control/Control.h"
#include "control/SharedVariable.h"
#include "control/Powertrain.h"
#include "control/SpeedUpdater.h"
#include "control/SpeedSetpoint.h"
#include "control/SpeedSamples.h"
#include "control/MotionCommand.h"


using namespace std::chrono_literals;


/**
 * Class responsible for robot's movements. It translates the desired movements
 * orders into commands for the powertrain.
 */
template<
    typename kinematics_t,
    typename powertrain_t>
class Control::MotionController
{
    using data_t = float;
    using speed_updater_t = Control::SpeedUpdater<data_t, kinematics_t>;
    using speed_setpoint_t = Control::SpeedSetpoint<speed_updater_t>;
    using powertrain_control_t = typename powertrain_t::control_t;
    using powertrain_task_t = Miloco::ReducedObsPolePlacement<
        speed_setpoint_t,                           // Setpoint type
        powertrain_t&,                              // Actuators type
        powertrain_t&,                              // Sensors type
        typename powertrain_control_t::k_matrix_t,  // Feedback matrix type
        powertrain_control_t&>;                     // Observer type

    public:
        explicit MotionController(
                kinematics_t& kinematics,
                powertrain_t& powertrain,
                powertrain_control_t& powertrainCtrl) :
            m_speedUpdater(
                    powertrainCtrl.m_deltaT,
                    powertrainCtrl.m_preGain(0,0),
                    powertrainCtrl.m_preGain(1,1),
                    kinematics),
            m_setpoint(m_speedUpdater),
            m_powertrain(powertrain),
            m_powertrainCtrlTask(
                    powertrainCtrl.m_deltaT,
                    m_setpoint,
                    1, // Pre-gain is not used by the control task. See Control::SpeedUpdater
                    m_powertrain,
                    m_powertrain,
                    powertrainCtrl,
                    powertrainCtrl.m_kMatrix) {
            m_taskExecuters[0] = std::make_unique<Miloco::Posix::TaskExecuter>(m_powertrainCtrlTask);
        }

        int
        start() {
            m_speedUpdater.stopMotors();
            m_powertrain.getOdometer().resetDistance();
            // Starting task executer
            return m_taskExecuters[0]->start(SCHED_FIFO, 20);
        }

        int
        stop() {
            // Stopping task executer
            return m_taskExecuters[0]->stop(1s);
        }

        void
        abortCmds() {
            m_speedUpdater.stopMotors();
        }

        void
        updateSpeed(Control::MotionCommand cmd) {
            std::cout << "Delta X: " << cmd.dX << std::endl;
            std::cout << "Delta Y: " << cmd.dY << std::endl;
            std::cout << "Delta O: " << cmd.dO << std::endl;
            std::cout << "Delta t: " << cmd.dt << std::endl;

            if (cmd.dt > 0) {
                m_speedUpdater.pushCmd(cmd);
            }
        }

        void
        adjPreGain(data_t preGainLeft, data_t preGainRight) {
            m_speedUpdater.setPreGain(preGainLeft, preGainRight);
        }

    private:
        speed_updater_t m_speedUpdater;
        speed_setpoint_t m_setpoint;
        powertrain_t& m_powertrain;
        powertrain_task_t m_powertrainCtrlTask;
        std::array<std::unique_ptr<Miloco::Posix::TaskExecuter>, 1> m_taskExecuters;
};
