/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstddef>


/**
 * Forward declarations of namespace Control
 */
namespace Control
{
    // Entities
    template<typename data_t>
    class SharedVariable;

    template<
        typename data_t,
        typename sensor_t>
    class DataPointsUpdater;

    template<
        typename speed_t,
        typename kinematics_t>
    class SpeedUpdater;

    template<typename updater_t>
    class SpeedSetpoint;

    template<typename data_updater_t>
    class MotionInterpolator;

    template<
        typename motor_t,
        typename encoder_t,
        typename data_t>
    class Powertrain;

    template<
        std::size_t stateVariables,
        std::size_t inputs,
        std::size_t outputs,
        typename data_t>
    struct PowertrainControl;

    template<
        std::size_t outputs,
        typename data_t>
    class Odometer;

    template<
        typename kinematics_t,
        typename powertrain_t>
    class MotionController;

    struct MotionCommand;

    struct SpeedSamples;
}
