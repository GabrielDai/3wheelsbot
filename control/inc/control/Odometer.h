/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <mutex>
#include <Eigen/Dense>
#include <kinematics/Wheel.h>
#include <tlv-mi/Serializable.h>

#include "control/Control.h"


/**
 * Entity used to save/read the distance travelled
 */
template<
    std::size_t outputs,
    typename data_t>
class Control::Odometer
{
    public:
        explicit Odometer(Kinematics::Wheel wheel) :
            m_totalDistLeft(0),
            m_totalDistRight(0),
            m_wheel(wheel) {
        }

        void
        addDistance(int distLeft, int distRight) {
            std::lock_guard<std::mutex> lock(m_access);
            m_totalDistLeft += distLeft;
            m_totalDistRight += distRight;
        }

        TLVMi::Serializable<float, 2>
        readDistance() {
            float values[2];
            {
                std::lock_guard<std::mutex> lock(m_access);
                values[0] = static_cast<float>(m_totalDistLeft);
                values[1] = static_cast<float>(m_totalDistRight);
            }
            values[0] *= m_wheel.radPerPulse * m_wheel.radius;
            values[1] *= m_wheel.radPerPulse * m_wheel.radius;

            TLVMi::Serializable<float, 2> dst(values);
            return dst;
        }

        void
        resetDistance() {
            std::lock_guard<std::mutex> lock(m_access);
            m_totalDistLeft = 0;
            m_totalDistRight = 0;
        }

    private:
        std::mutex m_access;
        int m_totalDistLeft;
        int m_totalDistRight;
        Kinematics::Wheel m_wheel;
};
