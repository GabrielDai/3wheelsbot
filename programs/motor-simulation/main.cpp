/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <cstdint>
#include <iostream>
#include <Eigen/Dense>
#include <miloco/ReducedObserver.h>
#include <miloco/filters/StateSpaceSys.h>
#include <platform/simulation/SimEncoder.h>
#include <platform/simulation/SimMotorActuator.h>


int main ()
{
    constexpr int stateVariables = 2;
    constexpr int inputs = 1;
    constexpr int outputs = 1;
    using sys_t = Miloco::StateSpaceSys<stateVariables, inputs, outputs, float>;

    // State matrix
    Eigen::Matrix<float, stateVariables, stateVariables> G;
    G <<  0.8404,  0.0036,
        -22.3592, 0.02613;

    // Input matrix
    Eigen::Matrix<float, stateVariables, inputs> H;
    H << 0.1452,
        20.3468;

    // Output matrix
    Eigen::Matrix<float, outputs, stateVariables> C;
    C << 1, 0;

    // Simulated plant
    sys_t plant(G, H, C);

    // Observer
    using observer_t = Miloco::ReducedObserver<stateVariables, 1, inputs, outputs, float>;

    Eigen::Matrix<float, 1, 1> Gaa;
    Gaa << G(0, 0);

    Eigen::Matrix<float, 1, 1> Gab;
    Gab << G(0, 1);

    Eigen::Matrix<float, 1, 1> Gba;
    Gba << G(1, 0);

    Eigen::Matrix<float, 1, 1> Gbb;
    Gbb << G(1, 1);

    Eigen::Matrix<float, 1, 1> Ha;
    Ha << H(0);

    Eigen::Matrix<float, 1, 1> Hb;
    Hb << H(1);

    Eigen::Matrix<float, 1, 1> Ke;
    Ke << 7.3387;

    observer_t obs(Gaa, Gab, Gba, Gbb, Ha, Hb, Ke);

    // Pole placement - K matrix
    Eigen::Matrix<float, 1, stateVariables> K;
    K << 3.5767, 0.0171;

    float preGain = 1/0.2139;

    // Encoder
    Simulation::SimEncoder encoder(plant);

    // Actuator
    Simulation::SimMotorActuator actuator(plant);

    Eigen::Matrix<float, 1, 1> input;
    input << 1;

    // Control
    for(uint8_t i = 0; i < 30; i++) {
        auto plantOutput = encoder.readOutput();

        auto obsState = obs.getStates(plantOutput);
        std::cout << "n1:" << obsState[0] << ":n2:" << obsState[1] << std::endl;

        auto feedback = K * obsState;
        auto plantInput = preGain * input - feedback;

        obs.updateStates(plantInput, plantOutput);
        actuator.setPlantInput(plantInput);
    }
    return 0;
}
