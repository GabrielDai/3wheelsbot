/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <cstddef>
#include <chrono>
#include <iostream>
#include <time.h>
#include <control/SharedVariable.h>
#include <miloco/filters/MovAverageFilter.h>
#include <miloco/tasks/StepResponse.h>
#include <miloco/posix/Utils.h>
#include <platform/beaglebone/BatteryReader.h>
#include <platform/beaglebone/MotorsDriver.h>
#include <platform/beaglebone/EncodersDriver.h>
#include <platform/beaglebone/MotorUnit.h>

#include "MotorStepResponse.h"
#include "MotorOutputLogger.h"

using shared_variable_t = Control::SharedVariable<double>;
using namespace std::chrono_literals;


MotorStepResponse::MotorStepResponse()
{
}

void
MotorStepResponse::run(
        unsigned int samples,
        int delay_us,
        uint8_t motor_ch,
        uint8_t encoder_ch)
{
    std::cout << "samples:" << samples << std::endl;
    std::cout << "delay:" << delay_us << std::endl;
    std::cout << "motor:" << static_cast<int>(motor_ch) << std::endl;
    std::cout << "encoder:" << static_cast<int>(encoder_ch) << std::endl;

    // Resources
    Beaglebone::BatteryReader battery;
    shared_variable_t sharedVariable;
    Beaglebone::MotorsDriver motors(sharedVariable);
    Beaglebone::EncodersDriver encoders;
    MotorOutputLogger logger(static_cast<size_t>(samples));

    auto& encoder = encoders.getEncoder(encoder_ch - 1);
    auto& actuator = motors.getMotorActuator(motor_ch - 1);

    Beaglebone::MotorUnit motor(encoder, actuator);

    std::chrono::microseconds chronoDelay = std::chrono::microseconds(delay_us);
    timespec delay = Utils::Time::chronoDurationToTimespec(chronoDelay);
    timespec remDelay;

    // miLoCo's task -- StepResponse
    Miloco::StepResponse<
        shared_variable_t&,
        Beaglebone::MotorUnit,
        Beaglebone::MotorUnit,
        MotorOutputLogger&> task(
                chronoDelay,
                sharedVariable,
                motor,
                motor,
                logger);

    // Current battery voltage measurement
    constexpr int filterSamples = 10;
    double batteryVoltage;

    Miloco::MovAverageFilter<
        double,
        filterSamples> filter;

    for (int i = 0; i < filterSamples; i++) {
        filter.init(battery.readValue());
        batteryVoltage = filter.marchOneStep(battery.readValue());
    }
    sharedVariable.updateValue(batteryVoltage);
    std::cout << "battery voltage:" << sharedVariable.readValue() << std::endl;

    // Taking samples of the motor's output
    task.setup();
    for (unsigned int i = 0; i < samples - 1; i++) {
        nanosleep(&delay, &remDelay);
        task.loop();
    }

    // Printing samples
    std::cout << "output:";
    auto logs = logger.getLogs();
    for (auto i: logs) {
        std::cout << i << ":";
    }
    std::cout << std::endl;
}
