/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>

class MotorStepResponse
{
    public:
        explicit MotorStepResponse();

        void run(
            unsigned int samples,
            int delay_us,
            uint8_t motor_ch,
            uint8_t encoder_ch);
};
