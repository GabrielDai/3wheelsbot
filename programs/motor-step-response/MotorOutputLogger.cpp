/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "MotorOutputLogger.h"


MotorOutputLogger::MotorOutputLogger(size_t samples) :
    m_index(0),
    m_outputs(samples, 0)
{
}

void
MotorOutputLogger::save(int current_out)
{
    if (m_index < m_outputs.size()) {
        m_outputs[m_index] = current_out;
        m_index++;
    }
}

std::vector<int>
MotorOutputLogger::getLogs()
{
    return m_outputs;
}
