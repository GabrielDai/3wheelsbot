/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <stdlib.h>

#include "MotorStepResponse.h"

int limitOption(char option[], int minValue, int maxValue);
void printUse(char cmd[]);


int main (int argc, char* argv[])
{
    if (argc == 5) {
        unsigned int samples = static_cast<unsigned int>(limitOption(argv[1], 1, 1000000));
        int delay = static_cast<int>(limitOption(argv[2], 1, 1000000));
        uint8_t motor_ch = static_cast<uint8_t>(limitOption(argv[3], 0, 4));
        uint8_t encoder_ch = static_cast<uint8_t>(limitOption(argv[4], 0, 3));

        MotorStepResponse program;
        program.run(samples, delay, motor_ch, encoder_ch);
    } else {
        std::cerr << "Bad amount of arguments" << std::endl;
        printUse(argv[0]);
    }

    return 0;
}

int limitOption(char option[], int minValue, int maxValue)
{
    int limOption = atoi(option);
    limOption = std::min(maxValue, limOption);
    limOption = std::max(minValue, limOption);

    return limOption;
}

void printUse(char cmd[])
{
    std::cout << std::endl;
    std::cout << cmd << " [samples] [delay] [motor channel] [encoder channel]" << std::endl;
    std::cout << std::endl;
    std::cout << "[samples] amount of samples to take (from 1 to 1000000)" << std::endl;
    std::cout << "[delay] delay between samples in micro seconds (from 1 to 1000000)" << std::endl;
    std::cout << "[motor channel] number of motor output used on Beaglebone Blue (from 1 to 4)" << std::endl;
    std::cout << "[encoder channel] number of encoder input used on Beaglebone Blue (from 1 to 3)" << std::endl;
}
