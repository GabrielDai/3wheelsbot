/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstddef>
#include <vector>


class MotorOutputLogger
{
    public:
        explicit MotorOutputLogger(size_t samples);

        void save(int current_out);
        std::vector<int> getLogs();

    private:
        size_t m_index;
        std::vector<int> m_outputs;
};
