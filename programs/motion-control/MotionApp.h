/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <control/Powertrain.h>
#include <control/MotionController.h>
#include <miloco/filters/MovAverageFilter.h>
#include <miloco/tasks/InputPreprocessing.h>
#include <miloco/posix/TaskExecuter.h>
#include <platform/beaglebone/BatteryReader.h>
#include <platform/beaglebone/EncodersDriver.h>
#include <platform/beaglebone/MotorsDriver.h>
#include <platform/beaglebone/IMU.h>
#include <kinematics/TWheelsBot.h>


/**
 * Robot's movements application.
 */
class MotionApp
{
    public:
        explicit MotionApp();

        /**
         * Start the application
         */
        int startApp(float factorMotorLeft, float factorMotorRight);

        /**
         * Stop the application
         */
        int stopApp();

        /**
         * Give a new motion command to the robot. The command will be append
         * to a motion commands queue. In case you want to overwrite all
         * current queued commands, first you should call stopRobot() and
         * then send the new one.
         */
        void moveRobot(Control::MotionCommand cmd);

        /**
         * Stop the robot and clear all queued commands.
         */
        void stopRobot();

        /**
         * Return the travelled distance per wheel.
         * See Control::Odometer::readDistance()
         */
        TLVMi::Serializable<float, 2> readOdometer();

        /**
         * Reset the travelled distance.
         */
        void resetOdometer();

        /**
         * Enable/disable the voltage input of the motors logging
         */
        void toggleMotorsVinLogging();

        /**
         * Enable/disable the revolution of the motors logging
         */
        void toggleMotorsRevLogging();

        static Beaglebone::IMU* sImu;

    private:
        // Alias types
        using powertrain_t = Control::Powertrain<
            typename Beaglebone::MotorsDriver::motor_t,
            typename Beaglebone::EncodersDriver::encoder_t,
            float>;
        using control_t = powertrain_t::control_t;
        using observer_t = control_t::observer_t;
        using k_matrix_t = control_t::k_matrix_t;
        using pre_gain_t = control_t::pre_gain_t;

        using kinematics_t = Kinematics::TWheelsBot<
            Beaglebone::IMU>;
        using motion_controller_t = Control::MotionController<
            kinematics_t,
            powertrain_t>;

        using batt_filter_t = Miloco::MovAverageFilter<
            double,
            10 >;
        using batt_filtering_t = Miloco::InputPreprocessing<
            Beaglebone::BatteryReader&,
            batt_filter_t,
            Control::SharedVariable<double>&>;

        // Auxiliary functions
        observer_t createPlantObserver();
        k_matrix_t createKMatrix();
        pre_gain_t createPreGainMatrix();

        // Battery's voltage filtering task
        Beaglebone::BatteryReader mBattery;
        batt_filter_t mBattFilter;
        Control::SharedVariable<double> mFilteredBattery;
        batt_filtering_t mBattFiltering;
        Miloco::Posix::TaskExecuter mBattFilteringExecuter;

        // 3WheelsBot kinematics
        Beaglebone::IMU mImu;
        Kinematics::Wheel mWheel;
        kinematics_t mKinematics;

        // Hardware on beaglebone
        Beaglebone::MotorsDriver mMotorsDriver;
        Beaglebone::EncodersDriver mEncodersDriver;

        // Powertrain and Motioncontroller
        powertrain_t mPowertrain;
        control_t mPowertrainCtrl;
        motion_controller_t mMotionController;

        bool mVinLog;
        bool mRevLog;
};
