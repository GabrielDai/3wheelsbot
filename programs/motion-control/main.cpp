/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <array>
#include <functional>
#include <iostream>
#include <time.h>
#include <signal.h>
#include <string>
#include <comm/RemoteCtrlServer.h>
#include <comm/Send.h>
#include <control/MotionCommand.h>
#include "MotionApp.h"


static bool running = true;

void
termination_handler(int signum)
{
    (void) signum;
    running = false;
}

void
startDefaultMovements(MotionApp& app)
{
    std::cout << "Moving the bot with default motion commands" << std::endl;

    std::array<Control::MotionCommand, 4> commands;
    // {dX, dY, dO, dt}
    commands[0] = {0.3, 0, 0, 1};
    commands[1] = {0.0, 0.0, 1.5708, .7};
    commands[2] = {0.3, 0, 0, 1};
    commands[3] = {0.0, 0.0, 1.5708, .7};

    for (size_t i = 0; i < commands.size(); i++) {
        app.moveRobot(commands[i]);
    }

    timespec delay, rem_delay;
    delay = {0,100000000}; // 100ms

    while (running) {
        nanosleep(&delay, &rem_delay);
    }
}

void
startRemoteControl(MotionApp& app)
{
    // TLV dispatcher and callbacks subscriptions
    Comm::dispatcher_t tlvDispatcher(Comm::tlvManagerRx);

    tlvDispatcher.subscribeCallback(Comm::MsgType::MOTION_CMD,
            [&](Comm::dispatcher_t::tlv_t tlv) {
                TLVMi::Serializable<float, 4> obj(std::move(tlv->buffer));
                auto cmd = Control::MotionCommand::create(obj);
                app.moveRobot(cmd);});

    tlvDispatcher.subscribeCallback(Comm::MsgType::CLOSE_CONNECTION,
            [](Comm::dispatcher_t::tlv_t tlv) {
                running = false;});

    tlvDispatcher.subscribeCallback(Comm::MsgType::DISTANCE_RQS,
            [&](Comm::dispatcher_t::tlv_t tlv) {
                auto dst = app.readOdometer();
                app.resetOdometer();
                Comm::send(Comm::MsgType::DISTANCE_RES, dst);});

    tlvDispatcher.subscribeCallback(Comm::MsgType::MOTORS_VIN_LOG,
            [&](Comm::dispatcher_t::tlv_t tlv) {
                app.toggleMotorsVinLogging();});

    tlvDispatcher.subscribeCallback(Comm::MsgType::MOTORS_REV_LOG,
            [&](Comm::dispatcher_t::tlv_t tlv) {
                app.toggleMotorsRevLogging();});

    tlvDispatcher.subscribeCallback(Comm::MsgType::ABORT,
            [&](Comm::dispatcher_t::tlv_t tlv) {
                app.stopRobot();});

    // Starting remote control server
    Comm::RemoteCtrlServer server(65000);
    if(server.createService()) {
        if(server.waitClient()) {
            MotionApp::sImu->subscribeCallback([](){
                    float yaw[1];
                    yaw[0] = static_cast<float>(MotionApp::sImu->getYaw());
                    TLVMi::Serializable<float, 1> yawS(yaw);
                    Comm::send(Comm::MsgType::YAW_LOG, yawS);
            });

            timespec delay, rem_delay;
            delay = {0,100000000}; // 100ms

            while(running) {
                tlvDispatcher.verifyAndDistpatch();
                nanosleep(&delay, &rem_delay);
            }
        }
    }
}

int
main (int argc, char* argv[])
{
    int result = 0;

    // Sigaction
    struct sigaction action;
    action.sa_handler = &termination_handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGINT, &action, NULL);

    // Parsing arguments
    std::function<void(MotionApp&)> run = &startDefaultMovements;
    float factorMotorLeft = 0;
    float factorMotorRight = 0;

    if (argc == 2 || argc == 4) {
        std::string arg1(argv[1]);
        std::string option("rem");
        if (arg1.compare(option) == 0) {
            run = &startRemoteControl;
        }

        if (argc == 4) {
            std::string arg2(argv[2]);
            std::string arg3(argv[3]);
            factorMotorLeft = std::stof(arg2);
            factorMotorRight = std::stof(arg3);
        }
    }
    if (argc == 3) {
        std::string arg1(argv[1]);
        std::string arg2(argv[2]);
        factorMotorLeft = std::stof(arg1);
        factorMotorRight = std::stof(arg2);
    }

    // Starting app
    MotionApp app;
    if(app.startApp(
                factorMotorLeft,
                factorMotorRight) == 0) {
        run(app);
        result = app.stopApp();
        if (result < 0) {
            std::cerr << "Error stopping the app" << std::endl;
        } else {
            std::cout << std::endl << "\tCiao ツ" << std::endl;
        }
    } else {
        std::cerr << "Error starting the app" << std::endl;
        result = -1;
    }

    return result;
}
