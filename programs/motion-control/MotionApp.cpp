/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "MotionApp.h"

using namespace std::chrono_literals;


MotionApp::MotionApp() :
    mBattFilter(),
    mBattFiltering(
            500ms,
            mBattery,
            mBattFilter,
            mFilteredBattery),
    mBattFilteringExecuter(mBattFiltering),
    mWheel(1466, 0.0325),
    mKinematics(0.165, mWheel, mImu),
    mMotorsDriver(mFilteredBattery),
    mEncodersDriver(),
    mPowertrain(
            mMotorsDriver.getMotorActuatorPair(0, 1),
            mEncodersDriver.getEncoderPair(0, 1),
            mWheel),
    mPowertrainCtrl(
            10ms,
            createPlantObserver(),
            createKMatrix(),
            createPreGainMatrix()),
    mMotionController(
            mKinematics,
            mPowertrain,
            mPowertrainCtrl),
    mVinLog(false),
    mRevLog(false)
{
}

Beaglebone::IMU* MotionApp::sImu;

int
MotionApp::startApp(float factorMotorLeft, float factorMotorRight)
{
    int result;
    if (mImu.start()) {
        MotionApp::sImu = &mImu;

        result = mBattFilteringExecuter.start(SCHED_FIFO, 20);
        if (result == 0) {
            result = mMotionController.start();

            if (result == 0) {
                // TODO maybe this is not the best place to do this
                if (factorMotorLeft != 0 && factorMotorRight != 0) {
                    mMotionController.adjPreGain(factorMotorLeft, factorMotorRight);
                }
            }
        }
    }
    return result;
}

int
MotionApp::stopApp()
{
    int result = 0;
    int result1 = mMotionController.stop();
    int result2 = mBattFilteringExecuter.stop(1s);

    if (result1 < 0 || result2 < 0) {
        result = -1;
    }

    mImu.stop();

    return result;
}

void
MotionApp::moveRobot(Control::MotionCommand cmd)
{
    mMotionController.updateSpeed(cmd);
}

void
MotionApp::stopRobot()
{
    mMotionController.abortCmds();
}

TLVMi::Serializable<float, 2>
MotionApp::readOdometer()
{
    auto travelledDistance = mPowertrain.getOdometer().readDistance();

    return travelledDistance;
}

void
MotionApp::resetOdometer()
{
    mPowertrain.getOdometer().resetDistance();
}

void
MotionApp::toggleMotorsVinLogging()
{
    mVinLog = !mVinLog;
    mPowertrain.enableVinLogging(mVinLog);
}

void
MotionApp::toggleMotorsRevLogging()
{
    mRevLog = !mRevLog;
    mPowertrain.enableRevLogging(mRevLog);
}

MotionApp::observer_t
MotionApp::createPlantObserver()
{
    constexpr int stateVariables = 4;
    constexpr int inputs = 2;
    constexpr int outputs = 2;

    // State matrix
    Eigen::Matrix<float, stateVariables, stateVariables> G;
    G << 1,                    0, 9.9886e-03,           0,
         0,                    1,          0,  9.9886e-03,
        -6.2727e-05,           0, 9.9772e-01,           0,
         0,          -6.2727e-05,          0,  9.9772e-01;

    // Input matrix
    Eigen::Matrix<float, stateVariables, inputs> H;
    H << 2.8552e-07,          0,
                  0, 2.8552e-07,
         5.7082e-05,          0,
                  0, 5.7082e-05;

    // Output matrix
    Eigen::Matrix<float, outputs, stateVariables> C;
    C << 1, 0, 0, 0,
         0, 1, 0, 0;

    Eigen::Matrix<float, 2, 2> Gaa;
    Gaa << G(0, 0), G(0, 1),
           G(1, 0), G(1, 1);

    Eigen::Matrix<float, 2, 2> Gab;
    Gab << G(0, 2), G(0, 3),
           G(1, 2), G(1, 3);

    Eigen::Matrix<float, 2, 2> Gba;
    Gba << G(2, 0), G(2, 1),
           G(3, 0), G(3, 1);

    Eigen::Matrix<float, 2, 2> Gbb;
    Gbb << G(2, 2), G(2, 3),
           G(3, 2), G(3, 3);

    Eigen::Matrix<float, 2, 2> Ha;
    Ha << H(0, 0), H(0, 1),
          H(1, 0), H(1, 1);

    Eigen::Matrix<float, 2, 2> Hb;
    Hb << H(2, 0), H(0, 1),
          H(3, 0), H(3, 1);

    Eigen::Matrix<float, 2, 2> Ke;
    Ke << 1.05812974098386,                0,
                         0, 1.05812974098386;

    observer_t observer(Gaa, Gab, Gba, Gbb, Ha, Hb, Ke);

    return observer;
}

MotionApp::k_matrix_t
MotionApp::createKMatrix()
{
    k_matrix_t kMatrix;
    kMatrix << 3.5767,      0, 0.0171,      0,
                    0, 3.5767,      0, 0.0171;

    return kMatrix;
}

MotionApp::pre_gain_t
MotionApp::createPreGainMatrix()
{
    pre_gain_t preGain;
    preGain << 40.5,  0,
                0, 30.5;

    return preGain;
}
